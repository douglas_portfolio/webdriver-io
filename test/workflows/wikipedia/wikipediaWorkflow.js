const Wikipedia = {
	homePage: require('../../pageobjects/wikipedia/homepage.page'),
	mainPage: require('../../pageobjects/wikipedia/mainpage.page'),
}

class WikipediaWorkflow {
	searchFor(word) {
		Wikipedia['mainPage'].searchFor(word).clickOnSearchButton()
    }
}

module.exports = new WikipediaWorkflow()
