const Page = require('../page');

class HomePage extends Page {

    get url(){return 'https://www.wikipedia.org'}
    get inputPassword () { return $('#password') }
    get btnSubmit () { return $('button[type="submit"]') }

    chooseLanguage(language) {
        let languageElement = $(`//strong[text()="${language}"]/parent::a`)
        super.isVisible(languageElement)
        languageElement.click()
    }

    open () {
        return super.open(this.url);
    }
}

module.exports = new HomePage();
