const HomePage = require('../pageobjects/wikipedia/homepage.page')
const MainPage = require('../pageobjects/wikipedia/mainpage.page')
const WikipediaWorkflow = require('../workflows/wikipedia/wikipediaWorkflow')

describe('Validate the text of wikipedia body', () => {
	it('Given I go to the wikipedia link', () => {
		HomePage.open()
		browser.pause(100)
		expect(browser).toHaveUrl('https://www.wikipedia.org/')
	})

	it('When I choose the English Language', () => {
		HomePage.chooseLanguage('English')
		browser.pause(100)
	})

	it('And I search for Embraer', () => {
		WikipediaWorkflow.searchFor('Embraer')
		browser.pause(100)
	})

	it('Then the text "founded in 1969" must be found in the body of the article', ()=>{
		MainPage.isTextInTheBody('founded in 1969')
		browser.pause(100)
	})
})
