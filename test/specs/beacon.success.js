const Beacon = require('../pageobjects/beacon/beacon.page')
const BeaconWorkflow = require('../workflows/beacon/beaconWorkflows')

describe('Validate fields in the search', () => {
	it('Given I go to the Beacon page', () => {
		Beacon.open()
	})

	it('When I select the first result of LGA initials.', () => {
		BeaconWorkflow.grantAirportListIsGraterThanAMinimunValueAndSelectTheFirstOne(
			'LGA',
			4
		)
	})
})