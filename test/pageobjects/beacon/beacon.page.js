const Page = require('../page')

class BeaconPage extends Page {
	get searchAirportInputHome() {
		return $('//button[@data-test-id="landing/search-mro-btn"]')
	}

	get searchFrameAirportInput() {
		return $('#airport')
	}

	get autocompleteList() {
		return $('#autocompleteList')
	}

	get autocompleteFirstResult() {
		return $(
			'//button[@data-test-id="directory/search-autocomplete-first-result"]'
		)
	}

	get autocompleteResults() {
		return $$('//*[@id="autocompleteList"]/li')
	}

	clickOnSearchAirport() {
		super.isVisible(this.searchAirportInputHome).click()
		return this
	}

	waitForAutoCompleteList() {
		super.isVisible(this.autocompleteList)
		return this
	}

	clickOnFirstResult() {
		super.isVisible(this.autocompleteFirstResult).click()
		return this
	}

	searchForAirport(value) {
		super.isVisible(this.searchFrameAirportInput).setValue(value)
		this.searchFrameAirportInput.waitForDisplayed(
			30000,
			true,
			'List was not displayed after 30s'
		)
		return this
	}

	getNumberOfAutocompleteOptions() {
		return this.autocompleteResults.lenght
	}

	open() {
		super.open('https://beacon.works')
	}
}

module.exports = new BeaconPage()
