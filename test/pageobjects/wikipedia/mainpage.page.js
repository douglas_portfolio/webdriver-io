const Page = require('../page');

class WikipediaMainPage extends Page {

    get inputPassword () { return $('input[type="search"]') }
    get btnSearch () { return $('input[name="go"]') }
    get articleBody() {return $('#mw-content-text')}

    searchFor(value){
        super.isVisible(this.inputPassword).setValue(value);
        return this
    }

    clickOnSearchButton(){
        super.isVisible(this.btnSearch).click()
        return this
    }

    isTextInTheBody(text){
        super.containsText(this.articleBody, text)
        super.saveScreenshot('Embraer')
        return this
    }

}

module.exports = new WikipediaMainPage();
