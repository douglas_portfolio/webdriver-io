# README

This is the firt time I use mocha or webdriver io.
Although I have already used other javascript frameorks lik protractor + cucumber, and there are a way to use cucumber here with webdriver io, I choose webdriver io with mocha to get new skills while doing this test. So, instead I was reproved, I will appreciate a lot if you make me a code review. ;)

# Instructions to run

First of all, please install all the dependencies:

``` npm install ```

To run these tests you could use:

``` npm run test```

To choose which suite to run, please use:

``` npm run suite <beacon | wikipedia>```

Beacon `.failure` spec will fail propositally, because it is an exercise requirement.