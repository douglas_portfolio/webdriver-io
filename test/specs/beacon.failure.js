const Beacon = require('../pageobjects/beacon/beacon.page')
const BeaconWorkflow = require('../workflows/beacon/beaconWorkflows')

describe('Should not be able to select a impossible value', ()=>{
	it('Given I go to the Beacon page', () => {
		Beacon.open()
	})

	it('When I select the first result of XPTO123 initials.', () => {
		BeaconWorkflow.grantAirportListIsGraterThanAMinimunValueAndSelectTheFirstOne(
			'XPTO123',
			4
		)
	})
})