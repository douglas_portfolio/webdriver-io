const Beacon = require('../../pageobjects/beacon/beacon.page')

class BeaconWorkflow {
	grantAirportListIsGraterThanAMinimunValueAndSelectTheFirstOne(
		airportInitials,
		minimumNumberOfOptionsInList
	) {
		let value = Beacon.clickOnSearchAirport()
			.searchForAirport(airportInitials)
			.getNumberOfAutocompleteOptions()
        expect(value > minimumNumberOfOptionsInList)
        Beacon.saveScreenshot('Beacon')
        Beacon.clickOnFirstResult()
        Beacon.saveScreenshot('Beacon2')
	}
}

module.exports = new BeaconWorkflow()
