module.exports = class Page {

    open (urlTxt) {
        return browser.url(urlTxt)
    }

    isVisible(element){
        element.waitForExist()
        expect(element).toBeVisible()
        return element
    }

    containsText(element, text){
        expect(this.isVisible(element)).toHaveTextContaining(text)
    }

    saveScreenshot(scrrenshotFileName){
        browser.saveScreenshot(`proofartifacts/${scrrenshotFileName}.png`)
    }
}
